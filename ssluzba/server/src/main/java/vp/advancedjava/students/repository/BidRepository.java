package vp.advancedjava.students.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Bid;

@Component
public interface BidRepository extends JpaRepository<Bid, Long> {
	
	public List<Bid> findByAdvertismentId(Long advertismentId);

}	

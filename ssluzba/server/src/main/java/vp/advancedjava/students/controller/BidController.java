package vp.advancedjava.students.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.students.dto.BidDTO;
import vp.advancedjava.students.model.Bid;
import vp.advancedjava.students.service.AdvertismentService;
import vp.advancedjava.students.service.BidService;

@RestController
public class BidController {  
	
	
	     
	@Autowired
	BidService bidService;
	
	@Autowired
	AdvertismentService advertismentService;
	
	@GetMapping(value ="api/adds/{id}/bids")
	ResponseEntity<List<BidDTO>> findAll(@PathVariable Long id) {
		List<BidDTO> bidDTOs = bidService.findAll(id).stream()
				.map(BidDTO::new)
				.collect(Collectors.toList());
		return new ResponseEntity<>(bidDTOs, HttpStatus.OK);
	}
	
	@PostMapping(value ="api/bids")
	ResponseEntity<BidDTO> save(@RequestBody BidDTO bidDTO) {
		Bid bid = new Bid();
		System.out.println(bidDTO.getId() + " " + bidDTO.getUsername() + " " + bidDTO.getAmount() + bidDTO.getAdvertismentDTO());
		bid.setId(bidDTO.getId());
		bid.setUsername(bidDTO.getUsername());
		bid.setText(bidDTO.getText());
		bid.setAmount(bidDTO.getAmount());
		bid.setAdvertisment(advertismentService.findOne(bidDTO.getAdvertismentDTO().getId()));
		
		Bid savedBid = bidService.save(bid);
		
		return new ResponseEntity<>(new BidDTO(savedBid), HttpStatus.OK);
	}
}

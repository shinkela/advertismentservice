package vp.advancedjava.students.controller;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import vp.advancedjava.students.dto.AdvertismentDTO;
import vp.advancedjava.students.model.Advertisment;
import vp.advancedjava.students.service.AdvertismentService;

@RestController
public class AdvertismentController {
		
		@Autowired
		AdvertismentService advertismentService;
		
		@GetMapping(value = "api/adds")
		ResponseEntity<List<AdvertismentDTO>> findAll(@RequestParam (defaultValue = "", name ="title") String title, Pageable page) {
			Page<Advertisment> adds = advertismentService.findByTitle(title, page);
			
			List<AdvertismentDTO> dtos = adds.getContent().stream()
					.map(AdvertismentDTO::new)
					.collect(Collectors.toList());
			return new ResponseEntity<>(dtos, HttpStatus.OK);
		}
		
		@GetMapping(value ="api/adds/{id}")
		ResponseEntity<AdvertismentDTO> findOne(@PathVariable Long id) {
			Advertisment advertisment = advertismentService.findOne(id);
			
			if(advertisment == null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
			return new ResponseEntity<>(new AdvertismentDTO(advertisment), HttpStatus.OK);
		}
		@PreAuthorize("isAuthenticated()")
		@DeleteMapping(value ="api/adds/{id}")
		ResponseEntity<Void> delete(@PathVariable Long id) {
			Advertisment advertisment = advertismentService.findOne(id);
			
			if(advertisment == null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
			advertismentService.delete(id);
			
			return new ResponseEntity<>(HttpStatus.OK);
		}
		@PreAuthorize("isAuthenticated()")
		@PostMapping(value = "api/adds")
		ResponseEntity<AdvertismentDTO> save (@RequestBody AdvertismentDTO advertismentDTO) {
			Advertisment advertisment = new Advertisment();
			
			advertisment.setId(advertismentDTO.getId());
			advertisment.setTitle(advertismentDTO.getTitle());
			advertisment.setDescription(advertismentDTO.getDescription());
			advertisment.setPrice(advertismentDTO.getPrice());
			advertisment.setLocation(advertismentDTO.getLocation());
			advertisment.setReadme(advertismentDTO.getReadme());
			
			Advertisment savedAdd = advertismentService.save(advertisment);
			
			return new ResponseEntity<>(new AdvertismentDTO(savedAdd), HttpStatus.OK);
		}
		@PreAuthorize("isAuthenticated()")
		@PutMapping(value = "api/adds/{id}")
		ResponseEntity<AdvertismentDTO> save (@PathVariable Long id, @RequestBody AdvertismentDTO advertismentDTO) {
			Advertisment advertisment = advertismentService.findOne(id);
			
			if(advertisment == null) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
			
			advertisment.setId(advertismentDTO.getId());
			advertisment.setTitle(advertismentDTO.getTitle());
			advertisment.setDescription(advertismentDTO.getDescription());
			advertisment.setPrice(advertismentDTO.getPrice());
			advertisment.setLocation(advertismentDTO.getLocation());
			advertisment.setReadme(advertismentDTO.getReadme());
			
			Advertisment savedAdd = advertismentService.save(advertisment);
			
			return new ResponseEntity<>(new AdvertismentDTO(savedAdd), HttpStatus.OK);
		}
}

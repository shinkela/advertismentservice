package vp.advancedjava.students.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Advertisment;
import vp.advancedjava.students.repository.AdvertismentRepository;

@Component
public class AdvertismentService {
		
	@Autowired
	AdvertismentRepository advertismentRepository;
	
	public Page<Advertisment> findByTitle(String title, Pageable page) {
		return advertismentRepository.findByTitleContains(title, page);
	}
	
	public Advertisment findOne(Long id) {
		return advertismentRepository.findOne(id);
	}
	
	public void delete(Long id) {
		advertismentRepository.delete(id);
	}
	
	public Advertisment save (Advertisment advertisment) {
		return advertismentRepository.save(advertisment);
	}
	
}	

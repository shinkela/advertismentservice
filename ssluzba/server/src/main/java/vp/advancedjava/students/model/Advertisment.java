package vp.advancedjava.students.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Advertisment {
	
	@Id
	@GeneratedValue
	private Long id;
	private String title;
	private String description;
	private int price;
	private String location;
	private String readme;
	
	@OneToMany(fetch = FetchType.LAZY)
	private Set<Bid> bids = new HashSet<>();
	
	public Advertisment() {
		
	}
	
	
	public Advertisment(Long id, String title, String description, int price, String location, String readme) {
		super();
		this.id = id;
		this.title = title;
		this.description = description;
		this.price = price;
		this.location = location;
		this.readme = readme;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getReadme() {
		return readme;
	}

	public void setReadme(String readme) {
		this.readme = readme;
	}
	
	
}

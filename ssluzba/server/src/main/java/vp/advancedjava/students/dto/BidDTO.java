package vp.advancedjava.students.dto;

import vp.advancedjava.students.model.Bid;

public class BidDTO {
	
		private Long id;
		private String username;
		private String text;
		private int amount;
		private AdvertismentDTO advertismentDTO;
		
		public BidDTO() {
			
		}
		
		public BidDTO(Bid bid) {
			this.id = bid.getId();
			this.username = bid.getUsername();
			this.text = bid.getText();
			this.amount = bid.getAmount();
			this.advertismentDTO = new AdvertismentDTO(bid.getAdvertisment());
		}

		public BidDTO(Long id, String username, String text, int amount, AdvertismentDTO advertismentDTO) {
			super();
			this.id = id;
			this.username = username;
			this.text = text;
			this.amount = amount;
			this.advertismentDTO = advertismentDTO;
		}

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}

		public String getUsername() {
			return username;
		}

		public void setUsername(String username) {
			this.username = username;
		}

		public String getText() {
			return text;
		}

		public void setText(String text) {
			this.text = text;
		}

		public int getAmount() {
			return amount;
		}

		public void setAmount(int amount) {
			this.amount = amount;
		}

		public AdvertismentDTO getAdvertismentDTO() {
			return advertismentDTO;
		}

		public void setAdvertismentDTO(AdvertismentDTO advertismentDTO) {
			this.advertismentDTO = advertismentDTO;
		}
		
		
			
}

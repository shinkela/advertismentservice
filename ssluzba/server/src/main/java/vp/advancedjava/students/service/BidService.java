package vp.advancedjava.students.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Bid;
import vp.advancedjava.students.repository.AdvertismentRepository;
import vp.advancedjava.students.repository.BidRepository;

@Component
public class BidService {
		
	@Autowired
	BidRepository bidRepository;
	
	@Autowired
	AdvertismentRepository advertismentRepository;
	
	public List<Bid> findAll(Long advertismentId) {
		return bidRepository.findByAdvertismentId(advertismentId);
	}
	
	public void delete (Long id) {
		bidRepository.delete(id);
	}
	
	public Bid save (Bid bid) {
		if(bid.getAmount() > bid.getAdvertisment().getPrice()) {
			bid.getAdvertisment().setPrice(bid.getAmount());
			Bid savedBid = bidRepository.save(bid);
			
			return savedBid;
		}
		return null;
	}
}

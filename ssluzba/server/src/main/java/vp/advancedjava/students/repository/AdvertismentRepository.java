package vp.advancedjava.students.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;

import vp.advancedjava.students.model.Advertisment;

@Component
public interface AdvertismentRepository extends JpaRepository<Advertisment, Long> {
	
	public Page<Advertisment> findByTitleContains(String title, Pageable page);

}

package vp.advancedjava.students.model;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Bid {
	
	@Id
	@GeneratedValue
	private Long id;
	private String username;
	private String text;
	private int amount;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Advertisment advertisment;
	
	public Bid () {
		
	}

	public Bid(Long id, String username, String text, int amount, Advertisment advertisment) {
		super();
		this.id = id;
		this.username = username;
		this.text = text;
		this.amount = amount;
		this.advertisment = advertisment;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public Advertisment getAdvertisment() {
		return advertisment;
	}

	public void setAdvertisment(Advertisment advertisment) {
		this.advertisment = advertisment;
	}
	
}
